pipeline { 
  raster 
  { 
    primitive: TRIANGLES
    index_buffer_element_size: UINT32
  },

  depth 
  {
    func: LESS
  },


  shader { 
    vertex { 
      attributes { 
        /*
          Vertex layout required for this pipeline (for input assembler)

          fields: 

          POSITION: 	      	float3
          TEXCOORD: 	      	float2
          TEXCOORD[0 - 12]: 	float2
          COLOR:    		uint8[4]
          NORMAL:   		float3
          TANGENT:  		float4
          JOINT:    		float4
          WEIGHT:   		float 
          FLOAT: 		float
          FLOAT2: 		float2
          FLOAT3: 		float3
          FLOAT4: 		float4
        */

        POSITION : a_position
        TEXCOORD : a_uv
        COLOR    : a_color
      }, 

      uniforms {
        mat4 u_mvp;
        float u_elapsed;
        //steepness must be between 0 and 1
        vec4 u_wave_A; //(dir.x, dir.y, steepness, wavelength)
        vec4 u_wave_B;
        vec4 u_wave_C;
        vec4 u_wave_D;
      },

      out {
        vec3 normal;
        vec2 uv;
        vec3 position;
      },

      code {
        //#define PI 3.1415926535897932384626433832795
        #define PI 3.14159265359f
        vec3 wave(vec4 dsw, vec3 point, inout vec3 tangent, inout vec3 binormal) {
          float k = 2 * PI / dsw.w;
          float c = sqrt(9.8f/k);
          vec2 d = normalize(dsw.xy);
          float f = k*(dot(d, point.xz) - c*u_elapsed);
          float a = dsw.z / k;

          tangent += vec3(
            -d.x * d.x * (dsw.z * sin(f)),
            d.x * (dsw.z * cos(f)),
            -d.x * d.y * (dsw.z * sin(f))
          );
          binormal += vec3(
            -d.x * d.y * (dsw.z * sin(f)),
            d.y * (dsw.z * cos(f)),
            -d.y * d.y * (dsw.z * sin(f))
          );

          return vec3(
            d.x * a * cos(f),
            a * sin(f),
            d.y * a * cos(f)
          );
        }

        void main() {
          position = a_position;
          vec3 tangent = vec3(1,0,0);
          vec3 binormal = vec3(0,0,1);
          position += wave(u_wave_A, a_position, tangent, binormal);
          position += wave(u_wave_B, a_position, tangent, binormal);
          position += wave(u_wave_C, a_position, tangent, binormal);
          position += wave(u_wave_D, a_position, tangent, binormal);
          gl_Position = u_mvp * vec4(position, 1.0);
          normal = normalize(cross(binormal,tangent));
          //normal = cross(binormal,tangent);
          uv = a_uv;
        }
      }
    },

    fragment { 
      uniforms {
        vec3 u_light_dir;
        vec3 u_light_color;
        vec3 u_view_pos;
      }, 

      out {
        vec4 frag_color; 
      },

      code {
        void main() {
          vec3 norm = normalize(normal);
          //vec3 norm = normal;

          // ambient light
          vec3 ambient = vec3(0.1,0.1,0.1);

          // diffuse light (sun)
          vec3 light = normalize(u_light_dir);
          vec3 diffuse = max(dot(norm,light),0.0) * u_light_color;

          // specular light
          float specular_strength = 1.0;
          vec3 view_dir = normalize(u_view_pos - position);
          vec3 reflect_dir = reflect(-light, norm);
          float spec = pow(max(dot(view_dir, reflect_dir),0.0), 32);
          //float spec = pow(dot(view_dir, reflect_dir), 32);
          vec3 specular = specular_strength * spec * u_light_color;

          // rim light
          float rim_val = 1.0 - max(dot(view_dir,norm),0.0);
          vec3 rim = vec3(smoothstep(0.8,1.0,rim_val));

          vec4 albedo = vec4(0.1,0.3,0.6,1);
          frag_color = vec4(ambient+diffuse+specular+rim,1.0) * albedo;
          //frag_color = vec4(norm,1.0);
          //frag_color = vec4(uv,0.0,1.0);
          //frag_color = vec4(rim,1.0);
        }
      }
    } 
  } 
} 
