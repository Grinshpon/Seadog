#define GS_IMPL
#include <gs/gs.h>

#define GS_GFXT_IMPL
#include <gs/util/gs_gfxt.h>

#define GS_IMMEDIATE_DRAW_IMPL
#include <gs/util/gs_idraw.h>

#define WAVES_IMPL
#include "waves.h"

#define INLINE static inline

#include "data.h"
#include "util.h"

#define GAME_WIDTH 900
#define GAME_HEIGHT 580

gs_immediate_draw_t gsi = {0};
//int frame = 0;

gs_vec2 crosshair = {0};
gs_asset_texture_t crosshair_tex = {0};

model_t floor_tile;
gs_gfxt_texture_t floor_tex = {0};

void app_init() {
  app_t* app = gs_user_data(app_t);
  app->cb = gs_command_buffer_new();
  app->asset_dir = gs_platform_dir_exists("./assets") ? "./assets" : "../assets";
  char TMP[256] = {0};
  gsi = gs_immediate_draw_new();

  app->fbo = gs_graphics_framebuffer_create(NULL);
  app->rt = gs_graphics_texture_create(
    &(gs_graphics_texture_desc_t) {
      .width = GAME_WIDTH,
      .height = GAME_HEIGHT,
      .format = GS_GRAPHICS_TEXTURE_FORMAT_RGBA8,
      .wrap_s = GS_GRAPHICS_TEXTURE_WRAP_REPEAT,
      .wrap_t = GS_GRAPHICS_TEXTURE_WRAP_REPEAT,
      .min_filter = GS_GRAPHICS_TEXTURE_FILTER_NEAREST,
      .mag_filter = GS_GRAPHICS_TEXTURE_FILTER_NEAREST
    }
  );
  app->depth = gs_graphics_texture_create(
    &(gs_graphics_texture_desc_t) {
      .width = GAME_WIDTH,
      .height = GAME_HEIGHT,
      .format = GS_GRAPHICS_TEXTURE_FORMAT_DEPTH32F,
      
      .min_filter = GS_GRAPHICS_TEXTURE_FILTER_NEAREST,
      .mag_filter = GS_GRAPHICS_TEXTURE_FILTER_NEAREST
    }
  );
  app->rp = gs_graphics_renderpass_create(
    &(gs_graphics_renderpass_desc_t){
      .fbo = app->fbo,
      .color = &app->rt,
      .color_size = sizeof(app->rt),
      .depth = app->depth,
    }
  );

  // Load pipeline from resource file
  gs_snprintf(TMP, sizeof(TMP), "%s/%s", app->asset_dir, "pipelines/entity.sf");
  app->pip[0] = gs_gfxt_pipeline_load_from_file(TMP);

  gs_snprintf(TMP, sizeof(TMP), "%s/%s", app->asset_dir, "pipelines/tile.sf");
  app->pip[1] = gs_gfxt_pipeline_load_from_file(TMP);
  printf("depth func: %d (less = %d)\n", app->pip[1].desc.depth.func, GS_GRAPHICS_DEPTH_FUNC_LESS);

  // Create material using this pipeline
  floor_tile.mat = gs_gfxt_material_create(&(gs_gfxt_material_desc_t){
    .pip_func.hndl = &app->pip[1]
  });

  floor_tile.mesh = generate_plane(1001);

  gs_graphics_texture_desc_t desc = {
    .format = GS_GRAPHICS_TEXTURE_FORMAT_RGBA8,
    .wrap_s = GS_GRAPHICS_TEXTURE_WRAP_REPEAT,
    .wrap_t = GS_GRAPHICS_TEXTURE_WRAP_REPEAT,
    .min_filter = GS_GRAPHICS_TEXTURE_FILTER_NEAREST,
    .mag_filter = GS_GRAPHICS_TEXTURE_FILTER_NEAREST
  };
  gs_snprintf(TMP, sizeof(TMP), "%s/%s", app->asset_dir, "textures/water.png");
  floor_tex = gs_gfxt_texture_load_from_file(TMP, &desc, false, false);

  app->game.cam = gs_camera_perspective();
  //app->game.cam.transform.position.z = 10.f;
  app->game.cam.transform.position.y = 2.0f;
  app->game.cam.transform.rotation = gs_quat_from_euler(0.0f,45.0f,-30.0f);

  gs_platform_lock_mouse(gs_platform_main_window(), true);

  gs_snprintf(TMP, sizeof(TMP), "%s/%s", app->asset_dir, "textures/crosshair.png");
  gs_asset_texture_load_from_file(TMP, &crosshair_tex, &desc, false, false);
}

float lerp(float a, float b, float t) {
  t = gs_clamp(t, 0.f, 1.f);
  return (b-a)*t + a;
}

float lerp_angle(float a, float b, float t) {
  t = gs_clamp(t, 0.f, 1.f);
  float da = fmodf((b-a), 360.f);
  float dist = fmodf(2*da, 360.f) - da;
  return dist*t + a;
}

void transform_move(gs_vqs *tf, float x, float z, float dt) {
  if(z != 0.0f) {
    tf->position = gs_vec3_add(tf->position, gs_vec3_scale(gs_vqs_forward(tf),dt*z));
  } 
  if(x != 0.0f) {
    tf->position = gs_vec3_add(tf->position, gs_vec3_scale(gs_vqs_right(tf),dt*x));
  }
}

void game_update() {
  app_t* app = gs_user_data(app_t);
  game_t *game = &app->game;
  float dt = game->dt;
  gs_vec2 fbs = game->fbs;
  gs_camera_t *cam = &game->cam;

  if (gs_platform_key_pressed(GS_KEYCODE_ESC)) {
    gs_quit();
  }

  const float speed = 15.0f;
  float want_move[2] = {0};
  if (gs_platform_key_down(GS_KEYCODE_W)) {
    want_move[1] += speed;
  }
  if (gs_platform_key_down(GS_KEYCODE_S)) {
    want_move[1] -= speed;
  }
  if (gs_platform_key_down(GS_KEYCODE_A)) {
    want_move[0] -= speed;
  }
  if (gs_platform_key_down(GS_KEYCODE_D)) {
    want_move[0] += speed;
  }
  if (gs_platform_key_down(GS_KEYCODE_R)) {
    cam->transform.position.y += speed*dt;
  }
  if (gs_platform_key_down(GS_KEYCODE_F)) {
    cam->transform.position.y -= speed*dt;
  }
  cam->transform.rotation = gs_quat_from_euler(0.0f,-crosshair.x, 0.0f);
  transform_move(&cam->transform, want_move[0], want_move[1], dt);

  const float sensitivity = 0.2f;
  gs_vec2 dp = gs_vec2_scale(gs_platform_mouse_deltav(), sensitivity);
  if(gs_vec2_len(dp) > 100.0f) {
    dp = gs_vec2_ctor(0,0);
  }
  crosshair = gs_vec2_add(crosshair, dp);

  //crosshair.x = gs_clamp(crosshair.x, -fbs.x/2.f, fbs.x/2.f);
  //crosshair.y = gs_clamp(crosshair.y, -fbs.y/2.f, fbs.y/2.f);

  cam->transform.rotation = gs_quat_from_euler(0.0f,-crosshair.x, -30.0f);
}

float fps_draw_timer = 0;
int fps_ticks = 0;
float fps_avg = 0;
float fps_num;
void fps_draw(float dt) {
  fps_draw_timer += dt;
  fps_ticks += 1;
  fps_avg += dt;
  if(fps_draw_timer >= 1.0f) {
    fps_draw_timer = 0;
    //fps_num = 1.0f/dt;
    fps_num = 1.0f/(fps_avg/(float)fps_ticks);
    fps_ticks = 0;
    fps_avg = 0;
  }
  char fps_text[8];
  gs_snprintf(fps_text, sizeof(fps_text), "%.2f", fps_num);
  gsi_text(&gsi, 1.0f, 1.0f, fps_text, NULL, false, 255,255,255,255);
}

void app_update()
{
  // Cache data for frame
  app_t* app = gs_user_data(app_t);
  gs_command_buffer_t* cb = &app->cb;

  const gs_vec2 fbs = gs_platform_framebuffer_sizev(gs_platform_main_window());
  //const float _t = simplify_euler(gs_platform_elapsed_time() * 0.05f);
  //const float dt = gs_platform_delta_time(); 
  app->game.fbs = fbs;
  app->game.dt = gs_platform_delta_time();
  app->game.elapsed = (float)(gs_platform_elapsed_time()/1000.0);

  game_update();

  gs_mat4 vp = gs_camera_get_view_projection(&app->game.cam, GAME_WIDTH, GAME_HEIGHT); 
  gs_mat4 v = gs_camera_get_view(&app->game.cam); 

  // Rendering
  //gs_graphics_clear_desc_t clear = {.actions = &(gs_graphics_clear_action_t){.color = {0.0f, 0.0f, 0.0f, 1.f}}};
  gs_graphics_clear_desc_t clear = {.actions = &(gs_graphics_clear_action_t){.color = {0.529f, 0.808f, 0.922f, 1.f}}};
  //gs_graphics_renderpass_begin(cb, app->rp);
  gs_graphics_renderpass_begin(cb, (gs_handle(gs_graphics_renderpass_t)){0});
  {
    // Set view port
    //gs_graphics_set_viewport(cb,0,0,GAME_WIDTH,GAME_HEIGHT);
    gs_graphics_set_viewport(cb,0,0,(int)fbs.x,(int)fbs.y);

    // Clear screen
    gs_graphics_clear(cb, &clear); 

    gs_vqs floor_tf = gs_vqs_default();
    gs_mat4 m = gs_vqs_to_mat4(&floor_tf);
    gs_mat4 mvp = gs_mat4_mul(vp,m);
    gs_gfxt_material_set_uniform(&floor_tile.mat, "u_mvp", &mvp);
    gs_gfxt_material_set_uniform(&floor_tile.mat, "u_elapsed", &app->game.elapsed);
    //float wave_a[] = {1.0f,1.0f, 0.5f, 50.f};
    //float wave_b[] = {0.0f,1.0f, 0.25f, 20.f};
    //float wave_c[] = {-1.0f,-1.0f, 0.08f, 10.f};
    float wave_a[] = {1.0f,1.0f, 0.5f, 300.f};
    float wave_b[] = {1.0f,0.0f, 0.2f, 160.f};
    float wave_c[] = {0.0f,1.0f, 0.2f, 80.f};
    float wave_d[] = {-1.0f,-1.0f, 0.08f, 40.f};
    gs_gfxt_material_set_uniform(&floor_tile.mat, "u_wave_A", wave_a);
    gs_gfxt_material_set_uniform(&floor_tile.mat, "u_wave_B", wave_b);
    gs_gfxt_material_set_uniform(&floor_tile.mat, "u_wave_C", wave_c);
    gs_gfxt_material_set_uniform(&floor_tile.mat, "u_wave_D", wave_d);

    //gs_gfxt_material_set_uniform(&floor_tile.mat, "u_tex", &floor_tex);
    float ld[] = {0.4f,1.f,0.f};
    float lc[] = {0.75f,0.75f,0.75f};
    gs_gfxt_material_set_uniform(&floor_tile.mat, "u_light_dir", ld);
    gs_gfxt_material_set_uniform(&floor_tile.mat, "u_light_color", lc);
    gs_gfxt_material_set_uniform(&floor_tile.mat, "u_view_pos", &app->game.cam.transform.position);
    gs_gfxt_material_bind(&app->cb, &floor_tile.mat);
    gs_gfxt_mesh_draw_pipeline(&app->cb, &floor_tile.mesh, &app->pip[1]);
  }
  //gs_graphics_renderpass_end(cb);

  gsi_camera2D(&gsi, (uint32_t)fbs.x, (uint32_t)fbs.y);
  //gsi_texture(&gsi, app->rt);
  uint32_t x = (uint32_t)fbs.x;
  uint32_t y = (uint32_t)fbs.y;
  //gsi_rect(&gsi, 0,y, x,0, 255, 255, 255, 255, GS_GRAPHICS_PRIMITIVE_TRIANGLES);
  
  fps_draw(app->game.dt);

  //gsi_camera2D(&gsi, (uint32_t)fbs.x, (uint32_t)fbs.y);
  //char angle_text[8];
  //gs_vec3 cam_f = gs_vqs_forward(&app->game.cam.transform);
  //float cam_angle = simplify_deg(gs_rad2deg(atan2(cam_f.x, cam_f.z))-180.f);
  //gs_snprintf(angle_text, sizeof(angle_text), "%.2f", cam_angle);
  //gsi_text(&gsi, 100.0f, 100.0f, angle_text, NULL, false, 255,255,255,255);

  //gs_graphics_renderpass_begin(cb, (gs_handle(gs_graphics_renderpass_t)){0});
    //gs_graphics_set_viewport(cb,0,0,(int)fbs.x,(int)fbs.y);
    //gs_graphics_clear(cb, &clear); 
    gsi_draw(&gsi, &app->cb);
  gs_graphics_renderpass_end(cb);

  //Submits to cb
  gs_graphics_command_buffer_submit(cb);
}

void app_shutdown() {
  // free
  app_t* app = gs_user_data(app_t);
  gs_command_buffer_free(&app->cb);
}

gs_app_desc_t gs_main(int32_t argc, char** argv) {
  return (gs_app_desc_t) {
    .user_data = gs_malloc_init(app_t),
    .init = app_init,
    .update = app_update,
    .shutdown = app_shutdown,
    .window.width = 900,
    .window.height = 580,
    .window.vsync = true,
  };
}
