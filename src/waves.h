#ifndef WAVES_H
#define WAVES_H

#include <gs/gs.h>
gs_gfxt_mesh_t generate_plane(int dim);

#endif
#ifdef WAVES_IMPL
#undef WAVES_IMPL

//dim: length and width in vertices
gs_gfxt_mesh_t generate_plane(int dim) {
  if(dim < 2) {
    dim = 2;
  }

  gs_gfxt_mesh_t mesh = gs_default_val();

  gs_dyn_array(gs_vec3) v_pos = NULL;
  //gs_v3(-1.0f, 0.0f, -1.0f), // Top Left
  //gs_v3(+1.0f, 0.0f, -1.0f), // Top Right 
  //gs_v3(-1.0f, 0.0f, +1.0f), // Bottom Left
  //gs_v3(+1.0f, 0.0f, +1.0f)  // Bottom Right

  // Vertex data for quad
  gs_dyn_array(gs_vec2) v_uvs = NULL;
  //gs_v2(0.0f, 0.0f),  // Top Left
  //gs_v2(1.0f, 0.0f),  // Top Right 
  //gs_v2(0.0f, 1.0f),  // Bottom Left
  //gs_v2(1.0f, 1.0f)   // Bottom Right

  gs_dyn_array(gs_vec3) v_norm = NULL;
  //gs_v3(0.f, 1.f, 0.0f),

  gs_dyn_array(gs_vec3) v_tan = NULL;
  //gs_v3(1.f, 0.f, 0.f),

  gs_dyn_array(gs_color_t) v_color = NULL;
  //GS_COLOR_WHITE,

  // Index data for quad
  gs_dyn_array(uint32_t) i_data = NULL;
  //0, 3, 2,    // First Triangle
  //0, 1, 3     // Second Triangle

  for(int y = 0; y < dim; y++) {
    for(int x = 0; x < dim; x++) {
      float fx = (float)x;
      float fy = (float)y;
      gs_dyn_array_push(v_pos, gs_v3(fx-(float)(dim-1)/2.f, 0.f, fy-(float)(dim-1)/2.f));
      gs_dyn_array_push(v_uvs, gs_v2(fx/(dim-1), fy/(dim-1)));
      gs_dyn_array_push(v_norm, gs_v3(0.f, 1.f, 0.0f));
      gs_dyn_array_push(v_tan, gs_v3(0.f, 0.f, 1.f));
      gs_dyn_array_push(v_color, GS_COLOR_WHITE);
    }
  }
  for(int y = 0; y < dim-1; y++) {
    for(int x = 0; x < dim-1; x++) {
      uint32_t idx0 = y*dim+x;
      uint32_t idx1 = y*dim+(x+1);
      uint32_t idx2 = (y+1)*dim+x;
      uint32_t idx3 = (y+1)*dim+(x+1);
      gs_dyn_array_push(i_data, idx0);
      gs_dyn_array_push(i_data, idx3);
      gs_dyn_array_push(i_data, idx2);

      gs_dyn_array_push(i_data, idx0);
      gs_dyn_array_push(i_data, idx1);
      gs_dyn_array_push(i_data, idx3);
    }
  }

  //for(int i = 0; i < gs_dyn_array_size(v_pos); i++) {
  //  printf("%.2f,%.2f,%.2f\n", v_pos[i].x, v_pos[i].y, v_pos[i].z);
  //}
  //for(int i = 0; i < gs_dyn_array_size(v_pos); i++) {
  //  printf("%.2f,%.2f\n", v_uvs[i].x, v_uvs[i].y);
  //}
  //for(int i = 0; i < gs_dyn_array_size(i_data)/3; i++) {
  //  printf("%d,%d,%d\n", i_data[3*i], i_data[3*i+1], i_data[3*i+2]);
  //}

  // Mesh data
  gs_gfxt_mesh_raw_data_t mesh_data = gs_default_val();

  //printf("verts in plane: %d\n", gs_dyn_array_size(v_pos));
  //printf("indices in plane: %d\n", gs_dyn_array_size(i_data));
  //printf("sizeof v_pos: %d\n", sizeof(gs_vec3)*gs_dyn_array_size(v_pos));
  // Primitive to upload
  gs_gfxt_mesh_vertex_data_t vert_data = gs_default_val();
  vert_data.positions.data = v_pos; vert_data.positions.size = sizeof(gs_vec3)*gs_dyn_array_size(v_pos);
  vert_data.normals.data = v_norm; vert_data.normals.size = sizeof(gs_vec3)*gs_dyn_array_size(v_norm);
  vert_data.tangents.data = v_tan; vert_data.tangents.size = sizeof(gs_vec3)*gs_dyn_array_size(v_tan);
  vert_data.colors[0].data = v_color; vert_data.colors[0].size = sizeof(gs_color_t)*gs_dyn_array_size(v_color);
  vert_data.tex_coords[0].data = v_uvs; vert_data.tex_coords[0].size = sizeof(gs_vec2)*gs_dyn_array_size(v_uvs);
  vert_data.indices.data = i_data; vert_data.indices.size = sizeof(uint32_t)*gs_dyn_array_size(i_data);
  vert_data.count = gs_dyn_array_size(i_data);

  // Push into primitives
  gs_dyn_array_push(mesh_data.primitives, vert_data); 

  gs_gfxt_mesh_desc_t mdesc = gs_default_val();
  mdesc.meshes = &mesh_data;
  mdesc.size = 1 * sizeof(gs_gfxt_mesh_raw_data_t);
  mdesc.keep_data = true;

  mesh = gs_gfxt_mesh_create(&mdesc);
  mesh.desc = mdesc;

  // Free data
  gs_dyn_array_free(mesh_data.primitives);

  gs_dyn_array_free(v_pos);
  gs_dyn_array_free(v_uvs);
  gs_dyn_array_free(v_norm);
  gs_dyn_array_free(v_tan);
  gs_dyn_array_free(v_color);
  gs_dyn_array_free(i_data);

  return mesh;
}

#endif
