//common typedefs and macros
#ifndef UTIL_H
#define UTIL_H

#include <gs/gs.h>

typedef const char * str;
typedef void(*callback)(void *);


#define GS_2PI (2.0f*GS_PI)

const float EPSILON = 0.0001;

INLINE
bool approx(float x, float y) {
  return (x-y) < EPSILON;
}

float simplify_deg(float t) {
  while(t > 360.f) {
    t-=360.f;
  }
  while(t < 0.f) {
    t+=360.f;
  }
  return t;
}

float simplify_rad(float t) {
  while(t > GS_2PI) {
    t-=GS_2PI;
  }
  while(t < 0.f) {
    t+=GS_2PI;
  }
  return t;
}

#endif
