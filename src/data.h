#ifndef DATA_H
#define DATA_H

typedef gs_vqs transform_t;

typedef struct {
  gs_gfxt_mesh_t mesh;
  gs_gfxt_material_t mat;
} model_t;

typedef struct {
  float dt;
  float elapsed;
  gs_vec2 fbs;
  gs_camera_t cam;
  gs_quat face_cam; //rotation for sprites to face the camera
} game_t;

typedef struct {
  gs_command_buffer_t cb;
  gs_gfxt_pipeline_t pip[2];
  gs_handle(gs_graphics_framebuffer_t) fbo;
  gs_gfxt_texture_t rt;
  gs_gfxt_texture_t depth;
  gs_handle(gs_graphics_renderpass_t) rp;
  game_t game;
  const char* asset_dir;
} app_t;

#endif
